### usage: image/draw package 
image/draw packageは画像処理のための標準ライブラリ
画像の合成に主に使用できる

出力する画像は次の式によって求められる

出力画像 = (元の画像 in mask) Op 入力する画像 

op: operator 計算方法みたいなもの

Porter Duffらの提唱する論文
-> 2つの画像を合成するには，12通りのやり方がある

golangだと2つのやり方によって12通りのやり方全てをカバーした？
src-over-dst と srcの２つ

draw packageのソースだとこれらは定数Over , Srcが宣言されている

#### geometric Alignment
出力画像はdstにsrcにmaskを掛け合わせたものを重畳してできる. 重畳するやり方はオペレータによって決まる.  

画像を合成する部分は必要なとこだけやれば良い
-> そっちの方が簡単だし早いよねって話

draw (dst, rectangle, src, sp, op)
draw関数は内部でdrawMask()を呼び出している. 
つまりラッパーですね
マスクをかけないdrawMask関数がdraw()てっこと
