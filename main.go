// Copyright 2015 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"flag"
	"fmt"
	"image"
	"image/color"
	"image/draw"
	_ "image/jpeg"
	"io/ioutil"
	"os"
	"time"

	"github.com/golang/freetype"
	"github.com/google/gxui"
	"github.com/google/gxui/drivers/gl"
	"github.com/google/gxui/samples/flags"
	yaml "gopkg.in/yaml.v2"
)

var (
	// blue = color.RGBA64{R: 0x0000, G: 0xffff, B: 0xffff, A: 0xffff}
	blue = color.RGBA{R: 0x00, G: 0xff, B: 0xff, A: 0xff}
	gr   = color.Gray16{Y: 0x4444}
	red  = color.RGBA{R: 0x00, G: 0xff, B: 0xff, A: 0xff}
	// back      = color.RGBA{R: 0x1e, G: 0x21, B: 0x32, A: 0xff}
	back      = color.RGBA{R: 30, G: 33, B: 59, A: 0xff}
	highlight = color.RGBA{R: 0xe9, G: 0x89, B: 0x89, A: 0xff}
	front     = color.RGBA{R: 0xd2, G: 0xd4, B: 0xde, A: 0xff}
)

var (
	colorMap = map[string]color.RGBA{
		"white": color.RGBA{R: 0xd2, G: 0xd4, B: 0xde, A: 0xff},
		// "white":   color.RGBA{R: 0xff, G: 0xff, B: 0xff, A: 0xff},
		"over":      color.RGBA{R: 0x95, G: 0xc4, B: 0xce, A: 0xff},
		"cyan":      color.RGBA{R: 0x00, G: 0xff, B: 0xff, A: 0xff},
		"highlight": color.RGBA{R: 0xe9, G: 0x89, B: 0x89, A: 0xff},
		"blue":      color.RGBA{R: 0x00, G: 0x00, B: 0xff, A: 0xff},
		"mazenta":   color.RGBA{R: 0xff, G: 0x00, B: 0xff, A: 0xff},
	}
)

const (
	// WindowWidth is image viewer width size
	WindowWidth = 2880

	// WindowHeight is image viewer height size
	WindowHeight = 1800
)

// gui application
func appMain(driver gxui.Driver) {

	// theme　はgxui.Theme を示す gxui.themeはインターフェース
	theme := flags.CreateTheme(driver)
	img := theme.CreateImage()

	// setting buttonNext
	buttonNext := theme.CreateButton()
	buttonNext.SetVerticalAlignment(gxui.AlignTop)
	buttonNext.SetText("next")

	// read files from ./text_slide directory
	// files is os.FileInfo slice

	// =============+ WARNING +================
	// @kid change readdir text_slide -> yaml_slide
	files, err := ioutil.ReadDir("./yaml_slide")
	if err != nil {
		fmt.Printf("can't read dir: %v\n", err)
		os.Exit(1)
	}

	// フォントファイルの読み込みとfreetype.Contextの初期化
	c, err := fontDrawerSetup()
	if err != nil {
		fmt.Printf("font drawer setup failed : %v\n", err)
		os.Exit(1)
	}

	var fileInd int = 0

	onClickFunc := func(gxui.MouseEvent) {

		s := Slide{}

		// initialize struct image.Image
		rgba := image.NewRGBA(image.Rect(0, 0, WindowWidth, WindowHeight))
		if err != nil {
			fmt.Printf("cant create NewRGBA image : %v\n", err)
		}
		draw.Draw(rgba, rgba.Bounds(), image.NewUniform(back), image.ZP, draw.Over)
		c.SetClip(rgba.Bounds())
		c.SetDst(rgba)
		c.SetSrc(image.NewUniform(colorMap["white"]))

		// read txt file
		// @kid change ./text_slide -> ./yaml_slide
		f, err := ioutil.ReadFile("./yaml_slide/" + files[fileInd].Name())
		if err != nil {
			fmt.Printf("cant read yml file : %v\n", err)
		}
		fmt.Println(files[fileInd].Name())

		// unmarshal yaml file
		err = yaml.Unmarshal(f, &s)
		if err != nil {
			fmt.Printf("failed unmarshal yaml file: %v\n", err)
		}
		title := s.Title

		// txt file parse

		// draw Title to rgba
		drawTitle(title, c, rgba)
		drawImage("./gopher2.png", rgba)

		c.SetFontSize(bodyFontSize)

		// draw body
		go func() {
			pt := freetype.Pt(80, WindowHeight/10+int(c.PointToFixed(bodyFontSize)>>6))
			ep := pt
			for _, body := range s.Body {
				fmt.Println(body.Color)

				// set character color
				if val, ok := colorMap[body.Color]; ok {
					c.SetSrc(image.NewUniform(val))
				}

				for _, mozi := range body.Text {
					ep, _ = c.DrawString(string(mozi), ep)

					time.Sleep(50 * time.Millisecond)
					driver.Call(func() {
						texture := driver.CreateTexture(rgba, 1)
						img.SetTexture(texture)
					})
				}
				// ep.X = pt.X
				// ep.Y += c.PointToFixed(60)
				// ep.Y += c.PointToFixed(fontsize)
			}
		}()

		fileInd++
		if fileInd > len(files)-1 {
			fileInd = 0
		}
	}
	buttonNext.OnClick(onClickFunc)

	// window setting
	window := theme.CreateWindow(WindowWidth, WindowHeight, "Image viewer")

	// @kid
	// ウィンドウのサイズに合わせて画像の大きさも変えるか
	// 1 の場合しない　2の場合する
	window.SetScale(flags.DefaultScaleFactor)
	// window.SetPadding(math.Spacing{L: 10, R: 10, T: 10, B: 10})
	window.AddChild(img)
	window.AddChild(buttonNext)

	fmt.Println("test")
	window.OnClose(driver.Terminate)
	fmt.Println("driver.Terminate end")
}

func main() {
	flag.Parse()
	gl.StartDriver(appMain)
}
