package main

import (
	"fmt"
	"image"
	"image/draw"
	"io/ioutil"
	"os"

	"github.com/golang/freetype"
)

const (
	fontfile = "./RictyDiminished-Regular.ttf"
)

// setting for slide image
var (
	dpi = 72.0
	// spacing  = 5.0
	titleFontSize = 120.0
	bodyFontSize  = 80.0
	wonb          = true
)

type Slide struct {
	Title string
	Body  []struct {
		Color string
		Text  string
	}
}

// createTitle func draw title at top of slide
// default font size is 50pt
func drawTitle(title string, c *freetype.Context, img draw.Image) error {

	pt := freetype.Pt(WindowWidth/3, 30+int(c.PointToFixed(titleFontSize)>>6))
	c.SetFontSize(120)
	_, err := c.DrawString(title, pt)
	if err != nil {
		return err
	}
	return nil
}

// createText func draw text middle of slide
// default font size is 60pt
func drawBody(text string, c *freetype.Context, img image.Image) error {

	pt := freetype.Pt(120, 120+int(c.PointToFixed(titleFontSize)>>6))
	// c.SetFontSize(60)
	c.SetFontSize(100)
	// @kid c.Drawstringの動作確認のため一時的にコメントアウト
	// ep, err := c.DrawString(text, pt)
	// if err != nil {
	// 	return err
	// }
	fmt.Println(pt)

	ep := pt
	var err error
	fmt.Println(ep)
	for _, t := range text {
		ep, err = c.DrawString(string(t), ep)
		if err != nil {
			return err
		}
		fmt.Println(ep)
	}
	return nil
}

func drawImage(path string, dst draw.Image) error {

	src, err := readImage(path)
	if err != nil {
		return err
	}
	pt := image.Point{X: -WindowWidth / 2, Y: -WindowHeight / 2}
	// fmt.Println(src.Bounds())
	// fmt.Println(pt)
	draw.Draw(dst, dst.Bounds(), src, pt, draw.Over)

	return nil
}

func fontDrawerSetup() (*freetype.Context, error) {
	fontBytes, err := ioutil.ReadFile("./RictyDiminished-Regular.ttf")
	if err != nil {
		return nil, err
	}

	f, err := freetype.ParseFont(fontBytes)
	if err != nil {
		return nil, err
	}

	c := freetype.NewContext()
	c.SetDPI(dpi)
	c.SetFont(f)
	c.SetFontSize(titleFontSize)
	return c, nil
}

//
// func createImage draw image
func createImage(path string, dst image.Image) (image.Image, error) {
	src, err := readImage(path)
	if err != nil {
		return nil, err
	}
	rgba := image.NewRGBA(image.Rect(0, 0, WindowWidth, WindowHeight))

	// 元
	draw.Draw(rgba, rgba.Bounds(), dst, image.ZP, draw.Over)

	// draw point
	pt := image.Point{X: -WindowWidth / 2, Y: -WindowHeight / 2}
	fmt.Println(src.Bounds())
	fmt.Println(pt)
	draw.Draw(rgba, rgba.Bounds(), src, pt, draw.Over)

	return rgba, nil
}

func createImageAno(path string, dst draw.Image) error {
	src, err := readImage(path)
	if err != nil {
		return err
	}
	pt := image.Point{X: -WindowWidth / 2, Y: -WindowHeight / 2}
	fmt.Println(src.Bounds())
	fmt.Println(pt)
	draw.Draw(dst, dst.Bounds(), src, pt, draw.Over)

	return nil
}

// text file 読み込み
func readText(file string) ([]byte, error) {
	f, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}

	return f, nil
}

func readImage(path string) (image.Image, error) {

	// os Open return *file struct
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	img, _, err := image.Decode(f)
	if err != nil {
		return nil, err
	}
	return img, nil
}
